#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

dists=("signalcli")

for dist in "${dists[@]}"; do
    for dir in "${CI_PROJECT_DIR}"/signal-cli-*-*; do
        export SCAN_DIR="${dir}"
        if [[ "${dir}" =~ ^.*signal-cli-.*-.*-testing$ ]]; then
            export COMPONENT="testing"
        fi
        curl -sL "https://gitlab.com/packaging/utils/-/raw/${UTILS_BRANCH:-main}/repo.sh" \
            | bash -s -- "${dist}";
    done;
done
