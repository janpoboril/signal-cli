#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-v0.0.0+0}"
version="${tag%%+*}"
iteration="${tag##*+}"
if [[ "${version}" =~ ^@.*$ ]]; then
    signal_cli_branch="${version/@/}"
fi
graalvm="${GRAALVM:-22.3.3}"
java="${JAVA:-java17}"
architecture="${ARCH:-amd64}"
libsignalclient_version="${LIBSIGNAL_CLIENT_VERSION:-0.25.0}"
libsignalclient_download_url="https://gitlab.com/packaging/libsignal-client/-/jobs/artifacts/v${libsignalclient_version}/raw/libsignal-client/${architecture}/libsignal_jni.so?job=libsignal-client-${architecture}"

apt-get -qq update
apt-get -qqy install \
    ruby-dev \
    ruby-ffi \
    curl \
    git \
    binutils \
    zip
type fpm >/dev/null 2>&1 || (
    gem install fpm
)

tmpdir="$(mktemp -d)"

case "${NAME}" in
"signal-cli-native")
    apt-get -y install \
        build-essential \
        libz-dev
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - native build"
    case "${architecture}" in
    "arm64")
        graalvm_arch="aarch64"
        ;;
    esac
    mkdir -p "${tmpdir}/usr/bin"
    if [ ! -d "/tmp/graalvm-ce-${java}-${graalvm}" ]; then
        curl -Lo "/tmp/graalvm-ce-${java}-linux-${graalvm_arch:-${architecture}}-${graalvm}.tar.gz" \
            "https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-${graalvm}/graalvm-ce-${java}-linux-${graalvm_arch:-${architecture}}-${graalvm}.tar.gz"
        tar -C /tmp -xzf "/tmp/graalvm-ce-${java}-linux-${graalvm_arch:-${architecture}}-${graalvm}.tar.gz"
    fi
    export PATH="/tmp/graalvm-ce-${java}-${graalvm}/bin:${PATH}"
    export JAVA_HOME="/tmp/graalvm-ce-${java}-${graalvm}"
    if [ ! -d /tmp/signal-cli ]; then
        git clone -b "${signal_cli_branch:-${version}}" https://github.com/AsamK/signal-cli /tmp/signal-cli
    fi
    cd /tmp/signal-cli
    find "${CI_PROJECT_DIR}/.patches" -type f -name '*.diff' -print0 | xargs --null -L1 git apply
    if [ -n "${REFLECT_CONFIG}" ]; then
        curl -L "${REFLECT_CONFIG}" \
            >/tmp/signal-cli/graalvm-config-dir/reflect-config.json
    fi
    if [ ! -f lib/src/main/resources/libsignal_jni.so ]; then
        curl -Lo lib/src/main/resources/libsignal_jni.so "${libsignalclient_download_url}"
    fi
    if [ ! -f build/native/nativeCompile/signal-cli ]; then
        ./gradlew nativeCompile
    fi
    mkdir -p "${tmpdir}/usr/bin"
    cp -v build/native/nativeCompile/signal-cli "${tmpdir}/usr/bin/signal-cli-native"
    cd -
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.after-install.sh" >/tmp/after-install.sh
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.after-upgrade.sh" >/tmp/after-upgrade.sh
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.before-remove.sh" >/tmp/before-remove.sh
    sed -i 's,%ORDER%,10,' /tmp/after-install.sh /tmp/after-upgrade.sh
    fpm_source_dirs="usr"
    fpm_opts="--after-install /tmp/after-install.sh"
    fpm_opts+=" --after-upgrade /tmp/after-upgrade.sh"
    fpm_opts+=" --before-remove /tmp/before-remove.sh"
    fpm_opts+=" --before-upgrade ${CI_PROJECT_DIR}/.packaging/before-upgrade.sh"
    fpm_opts+=" --provides signal-cli"
    fpm_opts+=" --deb-suggests sqlite3"
    ;;
"signal-cli-jre")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java"
    if [ ! -d "/tmp/signal-cli-${version//v/}" ]; then
        curl -Lo "/tmp/signal-cli-${version//v/}-Linux.tar.gz" \
            "https://github.com/AsamK/signal-cli/releases/download/${version}/signal-cli-${version//v/}-Linux.tar.gz"
        tar -C /tmp -xzf "/tmp/signal-cli-${version//v/}-Linux.tar.gz"
    fi
    case "${architecture}" in
    "arm64")
        libsignalclient="$(find "/tmp/signal-cli-${version//v/}/lib" -type f -name 'libsignal-client-*.jar')"
        if [ -z "${libsignalclient}" ]; then
            echo "unable to determine libsignal-client"
            exit 1
        fi
        curl -Lo /tmp/libsignal_jni.so "${libsignalclient_download_url}"
        zip -vj "${libsignalclient}" /tmp/libsignal_jni.so
        ;;
    esac
    mkdir -p \
        "${tmpdir}/usr/share/signal-cli" \
        "${tmpdir}/usr/bin"
    cd "/tmp/signal-cli-${version//v/}"
    sed 's,^APP_HOME=.*exit$,APP_HOME=/usr/share/signal-cli,' bin/signal-cli >"${tmpdir}/usr/bin/signal-cli-jre"
    chmod +x "${tmpdir}/usr/bin/signal-cli-jre"
    cp -rv lib "${tmpdir}/usr/share/signal-cli/"
    cd -
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.after-install.sh" >/tmp/after-install.sh
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.after-upgrade.sh" >/tmp/after-upgrade.sh
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.before-remove.sh" >/tmp/before-remove.sh
    sed -i 's,%ORDER%,20,' /tmp/after-install.sh /tmp/after-upgrade.sh
    fpm_opts="--depends ${java}-runtime-headless"
    fpm_opts+=" --after-install /tmp/after-install.sh"
    fpm_opts+=" --after-upgrade /tmp/after-upgrade.sh"
    fpm_opts+=" --before-remove /tmp/before-remove.sh"
    fpm_opts+=" --before-upgrade ${CI_PROJECT_DIR}/.packaging/before-upgrade.sh"
    fpm_opts+=" --provides signal-cli"
    fpm_opts+=" --deb-suggests sqlite3"
    fpm_source_dirs="usr"
    ;;
"signal-cli-dbus-service")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - DBus system service"
    architecture="all"
    mkdir -p \
        "${tmpdir}/etc/dbus-1/system.d" \
        "${tmpdir}/usr/share/dbus-1/system-services" \
        "${tmpdir}/etc/default"
    cp "${CI_PROJECT_DIR}/.packaging/org.asamk.Signal.conf" "${tmpdir}/etc/dbus-1/system.d/org.asamk.Signal.conf"
    curl -Lo "${tmpdir}/usr/share/dbus-1/system-services/org.asamk.Signal.service" "https://raw.githubusercontent.com/AsamK/signal-cli/${version}/data/org.asamk.Signal.service"
    cp -v "${CI_PROJECT_DIR}/.packaging/default-signal-cli-dbus" "${tmpdir}/etc/default/signal-cli-dbus"
    fpm_source_dirs="etc usr"
    fpm_opts="--depends signal-cli"
    fpm_opts+=" --depends systemd"
    fpm_opts+=" --deb-suggests dbus"
    fpm_opts+=" --deb-systemd ${CI_PROJECT_DIR}/.packaging/signal-cli-dbus.service"
    fpm_opts+=" --after-install ${CI_PROJECT_DIR}/.packaging/dbus-service-after-install.sh"
    ;;
"signal-cli-service")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - system service"
    architecture="all"
    mkdir -p \
        "${tmpdir}/etc/default"
    cp -v "${CI_PROJECT_DIR}/.packaging/default-signal-cli" "${tmpdir}/etc/default/signal-cli"
    fpm_source_dirs="etc"
    fpm_opts="--depends signal-cli"
    fpm_opts+=" --depends systemd"
    fpm_opts+=" --deb-systemd ${CI_PROJECT_DIR}/.packaging/signal-cli.service"
    ;;
esac
if [ -n "${signal_cli_branch}" ]; then
    tag="$(git -C /tmp/signal-cli describe --tags --dirty)"
    version="${tag/[a-zA-Z]/}-${signal_cli_branch}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}-testing"
else
    version="${version//[a-zA-Z]/}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}"
fi
mkdir -p "${output}"
# shellcheck disable=SC2086
fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name "${NAME}" \
    --package "${output}/${NAME}_${version}-${iteration}_${architecture}.deb" \
    --architecture "${architecture}" \
    --version "${version}" \
    --iteration "${iteration}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://github.com/AsamK/signal-cli/" \
    --description "${description}" \
    --deb-recommends morph027-keyring \
    --prefix "/" \
    --chdir "${tmpdir}" \
    --before-install "${CI_PROJECT_DIR}/.packaging/before-install.sh" \
    ${fpm_opts} \
    ${fpm_source_dirs}
