#!/bin/ash
# shellcheck shell=dash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

cleanup_error() {
  docker rm -f "signal-cli-native-builder-${CI_JOB_ID}"
  exit 1
}

docker rm -f "signal-cli-native-builder-${CI_JOB_ID}" || true
DOCKER_DEFAULT_PLATFORM="linux/${ARCH}" docker create --name "signal-cli-native-builder-${CI_JOB_ID}" \
  -e CI_PROJECT_DIR=/build \
  -e CI_COMMIT_TAG="${CI_COMMIT_TAG}" \
  -e CI_JOB_NAME="${CI_JOB_NAME}" \
  -e NAME="${NAME}" \
  -e ARCH="${ARCH}" \
  -e GRAALVM="${GRAALVM}" \
  -e JAVA="${JAVA}" \
  -e LIBSIGNAL_CLIENT_VERSION="${LIBSIGNAL_CLIENT_VERSION}" \
  -e DEBUG="${DEBUG}" \
  -e REFLECT_CONFIG="${REFLECT_CONFIG}" \
  ubuntu:focal /build/.gitlab-ci/build.sh
docker cp "${CI_PROJECT_DIR}" "signal-cli-native-builder-${CI_JOB_ID}":/build || cleanup_error
docker start "signal-cli-native-builder-${CI_JOB_ID}" || cleanup_error
docker attach "signal-cli-native-builder-${CI_JOB_ID}" || cleanup_error
docker cp "signal-cli-native-builder-${CI_JOB_ID}":/build /tmp/
loop=0
while docker inspect "signal-cli-native-builder-${CI_JOB_ID}" >/dev/null 2>&1
do
    if [ "${loop}" -gt 10 ]; then
        break
    fi
    loop="$((loop+1))"
    docker rm -f "signal-cli-native-builder-${CI_JOB_ID}" || true
done
cp -rv /tmp/build/"${CI_JOB_NAME}"* "${CI_PROJECT_DIR}"/
