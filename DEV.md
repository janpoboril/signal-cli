# Development

## Build

### cross-compile

```bash
DOCKER_DEFAULT_PLATFORM=linux/arm64 docker run --rm -it -v $PWD:/source -w /tmp --tmpfs /tmp:exec -e DEBUG=true -e CI_COMMIT_TAG=v0.10.2-1 -e CI_PROJECT_DIR=/tmp/source -e NAME=signal-cli-native -e ARCH=arm64 -e CI_JOB_NAME=signal-cli-native-arm64 ubuntu:jammy
cp -r /source . && /tmp/source/.gitlab-ci/build.sh
```
