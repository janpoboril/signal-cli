+++
title = "Backup"
weight = 4
+++

Sometimes you need to downgrade to an earlier version for whatever reasons and this will not work, if the database has been migrated to the new version already.

The databases per account are just simple sqlite3 files.

## Automatic

The standalone version 0.11.2-3 and higher is able to do automatic backups when apt is installing package upgrades. To do so, you'll need to create a file `/etc/default/signal-cli-backup` with the following content:

```bash
SIGNAL_CLI_USER=<signal-cli-user> # e.g. signal-cli when installed using docs
SIGNAL_CLI_DATA=</folder/to/signal-cli/data> # e.g. /var/lib/signal-cli/data when installed using docs
```

Also `sqlite3` command must be present in order to create the backups.

## Manual

To create a backup, just run the following command as the signal-cli user (manually or via cron):

```bash
find /folder/to/signal-cli/data -type f -name account.db | xargs -L1 -I{} sqlite3 {} ".backup {}.$(date +%Y%m%d%H%M)"
```
