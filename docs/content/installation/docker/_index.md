+++
title = "Docker installation"
+++

Docker setup recommended for JSON-RPC via TCP only (DBus won't work in this image).

## Run container

{{< tabs groupId="run" >}}
{{% tab name="Docker" %}}
```bash
docker run -d \
  --name signal-cli \
  --publish 7583:7583 \
  --volume /some/local/dir/signal-cli-config:/var/lib/signal-cli \
  --tmpfs /tmp:exec \
  registry.gitlab.com/packaging/signal-cli/signal-cli-<variant>:latest \
  daemon --tcp 0.0.0.0:7583
```
`<variant>`: `native` or `jre`.
{{% /tab %}}
{{% tab name="Docker Compose" %}}
```yaml
version: "3"
services:
  signal-cli:
    image: registry.gitlab.com/packaging/signal-cli/signal-cli-<variant>:latest
    command: daemon --tcp 0.0.0.0:7583
    ports:
      - "7583:7583"
    volumes:
      - "/some/local/dir/signal-cli-config:/var/lib/signal-cli"
    tmpfs:
      - "/tmp:exec"
```
`<variant>`: `native` or `jre`.
{{% /tab %}}
{{< /tabs >}}
