+++
title = "Registration"
weight = 2
+++

{{% notice info %}}
You will need a spare phone number to use for registration, **DO NOT USE YOUR CURRENT MOBILE PHONE NUMBER!**
SIP numbers are fine, just use `--voice` for verification code.
{{% /notice %}}

If the *signal-cli* user has been created by the package installation script, you'll need to adjust th commands to include `--config /var/lib/signal-cli`!

Example (see [CAPTCHA](https://github.com/AsamK/signal-cli/wiki/Registration-with-captcha)):

```bash
signal-cli --config /var/lib/signal-cli -a ACCOUNT register --voice --captcha <CAPTCHA>
signal-cli --config /var/lib/signal-cli -a ACCOUNT verify <VERIFICATION CODE>
```

For more details, see [signal-cli](https://github.com/AsamK/signal-cli#usage).
